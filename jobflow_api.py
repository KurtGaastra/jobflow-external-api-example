
def path(*args):
    return "/".join(args)


class JobFlowApi:
    def __init__(self, session, base_url):
        self.session = session
        self.template_url = path(base_url, 'job_templates')
        self.jobs_url = path(base_url, 'jobs')
        self.tasks_url = path(base_url, 'tasks')
        self.customer_url = path(base_url, 'customers')
        self.site_url = path(base_url, 'sites')

    def get_page(self, url, page=0, limit=100, params={}):
        """Retrieve a page of results"""
        params.update({"pageNum": page, "limit": limit})
        return self.session.get(url, params=params).json()

    def get_single(self, url):
        """Retrieve from an un-paged end point"""
        return self.session.get(url).json()

    def get_customers(self, page=0, limit=100):
        """Fetch a page of customers"""
        return self.get_page(self.customer_url, page, limit)

    def get_customer(self, uuid):
        """Full details of a single customer"""
        return self.get_single(path(self.customer_url, uuid))

    # Fetch list of sites for a customer
    def get_customer_sites(self, uuid):
        """Full site list for a single customer"""
        return self.get_single(path(self.customer_url, uuid, 'sites'))

    # Fetch a list of sites
    def get_sites(self, page=0, limit=100):
        """Fetch a page of sites"""
        return self.get_page(self.site_url, page, limit)

    # Fetch full details of a single site
    def get_site(self, uuid):
        """Full details for a single site"""
        return self.get_single(path(self.site_url, uuid))

    # Fetch full details of a single site
    def get_templates(self, page=0, limit=100):
        """Fetch a page of job templates"""
        return self.get_page(self.template_url, page, limit)

    # Fetch full details of a single site
    def get_template(self, uuid):
        """Full details for a single job tempalte"""
        return self.get_single(path(self.template_url, uuid))

    def create_job(self, job):
        return self.session.post(self.jobs_url, json=job).json()

    # Fetch list of jobs
    def get_jobs(self, page=0, limit=100, params={}):
        """Fetch a page of jobs"""
        return self.get_page(self.jobs_url, page, limit, params=params)

    # Fetch full details of a single job
    def get_job(self, uuid):
        """Full details for a single site"""
        return self.get_single(path(self.jobs_url, uuid))

    # Fetch full details of a single task
    def get_task(self, uuid):
        """Full details for a single site"""
        return self.get_single(path(self.tasks_url, uuid))
