from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from pprint import pprint

from jobflow_api import JobFlowApi

# Company identifier within jobflow
# This is the same company name used along with username and password when you log in to jobflow
company = ''

# Retrieved by creating a client application and generating a new secret
client_id = ''
client_secret = ''

base_url = 'https://prod.jobflow.nz/api/ext_api/{domain}/v1'.format(domain=company)

client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

token = oauth.fetch_token(token_url='https://prod.jobflow.nz/api/oauth/token', client_id=client_id,
                          client_secret=client_secret)

api = JobFlowApi(oauth, base_url)

customers = api.get_customers()['items']
pprint(customers)

# use first customer as an example
customer_uuid = customers[0]['uuid']

pprint(api.get_customer(customer_uuid))

pprint(api.get_customer_sites(customer_uuid))

# Fetch a list of sites
sites = api.get_sites()['items']
pprint(sites)

# Fetch site details
site_uuid = sites[0]['uuid']
pprint(api.get_site(site_uuid))

jobs = api.get_jobs(params={"phase": ["SCHEDULED", "COMPLETED"]})

pprint(jobs)
pprint(api.get_job(jobs['items'][0]['uuid']))
